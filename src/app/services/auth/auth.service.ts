import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from '../../classes/app-constants/app-constants';
import { LoginUser } from '../../models/login-user';
import { Observable } from 'rxjs';
import { Jwt } from 'src/app/models/jwt';
import { NewUser } from '../../models/new-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseURL: string;

  constructor(private httpClient: HttpClient) {
    this.baseURL = `${AppConstants.baseURL}/auth`;
  }

  public create(newUser: NewUser): Observable<any> {
    return this.httpClient.post<any>(`${this.baseURL}/create`, newUser);
  }

  public login(loginUser: LoginUser): Observable<Jwt> {
    return this.httpClient.post<any>(`${this.baseURL}/login`, loginUser);
  }

}
