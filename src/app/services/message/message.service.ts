import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private toastr: ToastrService) { }

  public success(message: string, timeOut: number): void {
    this.toastr.success(message, 'OK', { timeOut, positionClass: 'toast-top-center' });
  }

  public error(message: string, timeOut: number): void {
    this.toastr.error(message, 'Fail', { timeOut, positionClass: 'toast-top-center' });
  }

}
