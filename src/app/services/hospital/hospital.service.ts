import { Injectable } from '@angular/core';
import { AppConstants } from 'src/app/classes/app-constants/app-constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hospital } from '../../models/hospital';

@Injectable({
  providedIn: 'root'
})
export class HospitalService {

  private baseURL: string;

  constructor(private httpClient: HttpClient) {
    this.baseURL = `${AppConstants.baseURL}/hospital`;
  }

  public list(): Observable<Array<any>> {
    return this.httpClient.get<any>(`${this.baseURL}/list`);
  }

  public image(hospitalId: number): Observable<any> {
    return this.httpClient.get<any>(`${this.baseURL}/image/${hospitalId}`);
  }

  public create(newHospital: Hospital): Observable<any> {
    return this.httpClient.post<any>(`${this.baseURL}/create`, newHospital);
  }

  public find(id: number): Observable<any> {
    return this.httpClient.get<any>(`${this.baseURL}/find/${id}`);
  }

  public update(id: number, hospitalUpdated: any): Observable<any> {
    return this.httpClient.put<any>(`${this.baseURL}/update/${id}`, hospitalUpdated);
  }

  public updateImage(id: number, imageData: any): Observable<any> {
    return this.httpClient.put<any>(`${this.baseURL}/update/image/${id}`, imageData);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${this.baseURL}/delete/${id}`);
  }

}
