import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventEmiterService {

  @Output() userLoggedChanged: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  public emitUserLoggedChanged(userLogged: boolean): void {
    this.userLoggedChanged.emit(userLogged);
  }

}
