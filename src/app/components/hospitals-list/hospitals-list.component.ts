import { Component, OnInit } from '@angular/core';
import { faEye, faEdit, faTrashAlt, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HospitalService } from '../../services/hospital/hospital.service';
import { MessageService } from '../../services/message/message.service';
import { HospitalViewComponent } from '../modal/hospital-view/hospital-view.component';
import { ConfirmComponent } from '../modal/confirm/confirm.component';
import { TokenService } from '../../services/token/token.service';

@Component({
  selector: 'app-hospitals-list',
  templateUrl: './hospitals-list.component.html',
  styleUrls: ['./hospitals-list.component.scss']
})
export class HospitalsListComponent implements OnInit {

  public faEye = faEye;
  public faEdit = faEdit;
  public faTrashAlt = faTrashAlt;
  public faPlusCircle = faPlusCircle;

  public hospitalList: Array<any>;
  public isAdmin = false;

  constructor(private hospitalService: HospitalService,
              private messageService: MessageService,
              private modalService: NgbModal,
              private tokenService: TokenService) { }

  ngOnInit(): void {
    this.initializeComponent();
  }

  private initializeComponent(): void {
    this.loadHospitals();
    this.chekIfIsAdmin();
  }

  private chekIfIsAdmin(): void {
    const roles = this.tokenService.getAuthorities();
    roles.forEach(role => {
      if (role === 'ROLE_ADMIN') {
        this.isAdmin = true;
      }
    });
  }

  private loadHospitals(): void {
    this.hospitalService.list().subscribe(
      hospitalList => {
        this.hospitalList = hospitalList;
        console.log(this.hospitalList);
      },
      error => {
        this.messageService.error('Error on get hospitals list', 3000);
        console.error(error);
      }
    );
  }

  public onViewHospitalClick(hospital: any): void {
    console.log('hospital selected: ', hospital);

    this.hospitalService.image(hospital.id).subscribe(
      imageDto => {
        console.log(imageDto);

        const modalRef = this.modalService.open(HospitalViewComponent, { centered: true });
        modalRef.componentInstance.hospital = hospital;
        modalRef.componentInstance.imageDto = imageDto;
      },
      error => {
        this.messageService.error('Error on get hospital image', 3000);
        console.error(error);
      }
    );

  }

  public onDeleteHospitalClick(id: number): void {
    const modalRef = this.modalService.open(ConfirmComponent, { centered: true });
    modalRef.componentInstance.confirmationMessage = 'Are you sure that you want to delete this hospital?';
    modalRef.result.then(modalResult => {
      if (modalResult === 'yes') {
        this.deleteHospital(id);
      }
    });
  }

  private deleteHospital(id: number): void {
    this.hospitalService.delete(id).subscribe(
      data => {
        this.messageService.success(data.message, 3000);
        this.loadHospitals();
      },
      error => {
        this.messageService.error(error.error.message, 3000);
      }
    );
  }

}
