import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faEdit, faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { HospitalService } from '../../services/hospital/hospital.service';
import { MessageService } from '../../services/message/message.service';

@Component({
  selector: 'app-edit-hospital',
  templateUrl: './edit-hospital.component.html',
  styleUrls: ['./edit-hospital.component.scss']
})
export class EditHospitalComponent implements OnInit {

  public faEdit = faEdit;
  public faArrowAltCircleLeft = faArrowAltCircleLeft;

  private id: number;
  public name: string;
  public address: string;
  public hospital: any = null;
  public selectedFile: File = null;

  constructor(private activatedRoute: ActivatedRoute,
              private hospitalService: HospitalService,
              private messageService: MessageService,
              private router: Router) { }

  ngOnInit(): void {
    this.initializeComponent();
  }

  private initializeComponent(): void {
    this.id = this.activatedRoute.snapshot.params.id;
    this.hospitalService.find(this.id).subscribe(
      data => {
        this.hospital = data;
        this.name = this.hospital.name;
        this.address = this.hospital.address;
      },
      error => {
        this.messageService.error(error.error.message, 3000);
        this.router.navigate(['/']);
      }
    );
  }

  public onFileChanged(event: any): void {
    console.log(`new file: `, event);
    this.selectedFile = event.target.files[0];
  }

  public onSubmit(): void {
    this.hospital.name = this.name;
    this.hospital.address = this.address;

    console.log('THIS HOSPITAL: ', this.hospital);

    this.hospitalService.update(this.id, this.hospital).subscribe(
      data => {
        this.messageService.success(data.message, 3000);
        this.router.navigate(['/hospital/list']);
        if (this.selectedFile != null) {
          this.updateImage();
        }
      },
      error => {
        this.messageService.error(error.error.message, 3000);
      }
    );
  }

  public updateImage() {
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);

    this.hospitalService.updateImage(this.id, uploadImageData).subscribe(
      data => {
        this.messageService.success(data.message, 3000);
      },
      error => {
        this.messageService.error(error.error.message, 3000);
      }
    );
  }

}
