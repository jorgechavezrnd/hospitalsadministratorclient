import { Component, OnInit } from '@angular/core';
import { faHospital } from '@fortawesome/free-regular-svg-icons';
import { faSignInAlt, faDoorOpen, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { TokenService } from '../../services/token/token.service';
import { Router } from '@angular/router';
import { EventEmiterService } from '../../services/event-emiter/event-emiter.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public faHospital = faHospital;
  public faSignInAlt = faSignInAlt;
  public faDoorOpen = faDoorOpen;
  public faUserPlus = faUserPlus;

  public isLogged: boolean;

  constructor(private tokenService: TokenService,
              private router: Router,
              private eventEmiterService: EventEmiterService) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
    }

    this.eventEmiterService.userLoggedChanged.subscribe((userLogged: boolean) => {
      this.changeIsUserLogged(userLogged);
    });
  }

  public onLogout(): void {
    this.tokenService.logout();
    window.location.reload();
    this.router.navigate(['/']);
  }

  public changeIsUserLogged(isLogged: boolean): void {
    this.isLogged = isLogged;
  }

}
