import { Component, OnInit } from '@angular/core';
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { LoginUser } from '../../models/login-user';
import { TokenService } from '../../services/token/token.service';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { Jwt } from '../../models/jwt';
import { EventEmiterService } from '../../services/event-emiter/event-emiter.service';
import { MessageService } from '../../services/message/message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public faSignInAlt = faSignInAlt;

  public username: string;
  public password: string;
  public isLogged: boolean;

  public loginUser: LoginUser;

  constructor(private tokenService: TokenService,
              private authService: AuthService,
              private router: Router,
              private messageService: MessageService,
              private eventEmiterService: EventEmiterService) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
    }
  }

  public onLogin(): void {
    const loginUser: LoginUser = {
      username: this.username,
      password: this.password
    };

    this.authService.login(loginUser).subscribe(
      data => this.onLoginSucess(data),
      err => this.onLoginFail(err)
    );
  }

  private onLoginSucess(jwt: Jwt): void {
    this.tokenService.setToken(jwt.token);
    this.tokenService.setUserName(jwt.username);
    this.tokenService.setAuthorities(jwt.authorities);

    this.messageService.success(`Welcome ${jwt.username}`, 3000);

    this.eventEmiterService.emitUserLoggedChanged(true);

    this.router.navigate(['/']);
  }

  private onLoginFail(error: any): void {
    this.isLogged = false;
    const errorMessage = error.error.message === undefined
                         ? error.error
                         : error.error.message;
    this.messageService.error(errorMessage, 3000);
  }

}
