import { Component, OnInit } from '@angular/core';
import { faPlusCircle, faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { Hospital } from '../../models/hospital';
import { HospitalService } from '../../services/hospital/hospital.service';
import { MessageService } from '../../services/message/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-hospital',
  templateUrl: './new-hospital.component.html',
  styleUrls: ['./new-hospital.component.scss']
})
export class NewHospitalComponent implements OnInit {

  public faPlusCircle = faPlusCircle;
  public faArrowAltCircleLeft = faArrowAltCircleLeft;

  public name: string;
  public address: string;

  constructor(private hospitalService: HospitalService,
              private messageService: MessageService,
              private router: Router) { }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    console.log('submit');
    const newHospital: Hospital = {
      name: this.name,
      address: this.address
    };

    this.hospitalService.create(newHospital).subscribe(
      response => {
        this.messageService.success(response.message, 3000);
        this.router.navigate(['/hospital/list']);
      },
      error => {
        this.messageService.error(error.error.message, 3000);
      }
    );
  }

}
