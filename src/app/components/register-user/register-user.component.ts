import { Component, OnInit } from '@angular/core';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { TokenService } from 'src/app/services/token/token.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/services/message/message.service';
import { NewUser } from '../../models/new-user';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

  public faUserPlus = faUserPlus;

  public name: string;
  public username: string;
  public password: string;
  public isAdmin = true;

  public isLogged: boolean;

  constructor(private tokenService: TokenService,
              private authService: AuthService,
              private router: Router,
              private messageService: MessageService) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
    }
  }

  public onRegister(): void {
    const newUser: NewUser = {
      name: this.name,
      username: this.username,
      password: this.password,
      roles: this.isAdmin ? ['admin'] : []
    };

    this.authService.create(newUser).subscribe(
      data => this.onRegisterSuccess(data),
      error => this.onRegisterFail(error)
    );
  }

  private onRegisterSuccess(successMessage: any): void {
    this.messageService.success(successMessage.message, 3000);

    this.router.navigate(['/login']);
  }

  private onRegisterFail(error: any): void {
    this.messageService.error(error.error.message, 3000);
  }

}
