import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-hospital-view',
  templateUrl: './hospital-view.component.html',
  styleUrls: ['./hospital-view.component.scss']
})
export class HospitalViewComponent implements OnInit {

  @Input() hospital: any;
  @Input() imageDto: any;

  public hospitalImage: string = null;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.initializeComponent();
  }

  private initializeComponent(): void {
    if (this.imageDto.imageBytes) {
      const base64Data = this.imageDto.imageBytes;
      this.hospitalImage = `data:image/jpeg;base64,${base64Data}`;
    }
  }

}
