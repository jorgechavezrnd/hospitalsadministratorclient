import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './components/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HospitalsListComponent } from './components/hospitals-list/hospitals-list.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';

import { interceptorProvider } from './interceptors/data-interceptor/data-interceptor.service';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { HospitalViewComponent } from './components/modal/hospital-view/hospital-view.component';
import { NewHospitalComponent } from './components/new-hospital/new-hospital.component';
import { EditHospitalComponent } from './components/edit-hospital/edit-hospital.component';
import { ConfirmComponent } from './components/modal/confirm/confirm.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HospitalsListComponent,
    LoginComponent,
    HomeComponent,
    RegisterUserComponent,
    HospitalViewComponent,
    NewHospitalComponent,
    EditHospitalComponent,
    ConfirmComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent],
  entryComponents: [
    HospitalViewComponent,
    ConfirmComponent
  ]
})
export class AppModule { }
