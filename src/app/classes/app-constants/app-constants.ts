export class AppConstants {

    public static get baseURL(): string {
        return `${document.location.protocol}//${document.location.hostname}:8080`;
    }

}
