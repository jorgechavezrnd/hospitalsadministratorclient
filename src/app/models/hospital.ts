export interface Hospital {
    name: string;
    address: string;
}
