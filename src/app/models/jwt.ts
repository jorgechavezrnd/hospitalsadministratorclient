export interface Jwt {
    token: string;
    type: string;
    username: string;
    authorities: string[];
}
