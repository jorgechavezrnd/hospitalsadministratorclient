export interface NewUser {
    name: string;
    username: string;
    password: string;
    roles?: Array<string>;
}
