import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HospitalsListComponent } from './components/hospitals-list/hospitals-list.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { DataGuardService as guard } from './guards/data-guard/data-guard.service';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { NewHospitalComponent } from './components/new-hospital/new-hospital.component';
import { EditHospitalComponent } from './components/edit-hospital/edit-hospital.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register/user', component: RegisterUserComponent },
  { path: 'hospital/list', component: HospitalsListComponent, canActivate: [guard], data: { expectedRole: [ 'admin', 'user' ] } },
  { path: 'create/hospital', component: NewHospitalComponent, canActivate: [guard], data: { expectedRole: [ 'admin' ] } },
  { path: 'edit/hospital/:id', component: EditHospitalComponent, canActivate: [guard], data: { expectedRole: [ 'admin' ] } },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
